# Fixes before launching the tourism API

## harvests 

visit linköping 
strawberry 


## Model 

https://assets.entryscape.com/models/31/fields/109/overview - make sure the lookup field is working as it should, add schema:Landform and Guest Harbour. Ingen global sökning nu, ska gå mot både editera och data

Update trails examples

update trails lookup for stages 

fix event lookup - change from schema.org/event constraint to rdftype Event

check trails lookup from schema:Place 

remove Landform and include under schema:Place with additionaltype instead

add Activity entity type in local.js

fix Action, add terminologies and url pattern 

switch EducationalOrOccupationalCredential to Certification 

Legislation 

containsSection - kolla upp         

https://schema.org/ArtGallery

## Reusing entities 

## additional type to potential action 

keep old data.visitsweden.com/terms/ values for a transition period, tell all people working manually. 

## contained in place - contains place 

add storeConfig to data.visitsweden.com per property 

      },
      storeConfig: {
        includeLocal: false,
        endpoint: 'https://assets.entryscape.com/store',
        label: { en: 'Assets', sv: 'Assets SV' },
      },

### docs 

- Diagram

Place
Trail 
Evenemang

byt namn 

- Text 

search.md 

## script 

ersätt property checkin och checkout med script 

## contact 


