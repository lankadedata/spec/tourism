# Visit Sweden Information Model Changelog

## Version 2.1 

### fixed 
### changed 
- removed all data.visitsweden.com/terms activity types from schema:Place 
- changed cardinality on schema:address
- changed cardinality on dcterms:spatial 
- changed cardinality on schema:name for schema:ImageObject 
- remove lookup geoContains from schema:Place? due to spec difficulties
- 
### added 
- schema:Action class
- schema:potentialAction property   

- clarify that schema:ShoppingCenter is to be defined under schema:Place

## Version 2.0.1

### Changes
- Added new license schema for ImageObject:
  - Introduced Creative Commons license options:
    - CC BY 4.0 (Attribution)
    - CC BY-NC 4.0 (Attribution, Non-Commercial)
    - CC BY-NC-ND 4.0 (Attribution, Non-Commercial, No Derivative Works)
    - CC BY-NC-SA 4.0 (Attribution, Non-Commercial, Share Alike)
    - CC BY-ND 4.0 (Attribution, No Derivative Works)
    - CC BY-SA 4.0 (Attribution, Share Alike)
    - CC0 1.0 (Public Domain Dedication)
  - Added detailed bilingual descriptions for each license type
  - Implemented schema:license property with URI support

- Updated Place Relations:
  - Modified containedInPlace constraints to include:
    - schema:Place
    - schema:Landform
    - wikidata:Q283202 (Guest Harbour)
    - schema:Store
    - schema:LodgingBusiness
  - Updated containsPlace constraints to support:
    - schema:Place
    - schema:LodgingBusiness
    - schema:FoodEstablishment
    - schema:Store
    - wikidata:Q283202 (Guest Harbour)
  - Added improved descriptions and examples for place relationships
  - Enhanced documentation for hierarchical place structures

- Modified price range descriptions with new levels:
  - `$` - Budget-friendly
  - `$$` - Moderately priced
  - `$$$ ` - Expensive  
  - `$$$$` - Very expensive
  - Removed `$$$$$` tier for simplification

### Field Improvements
- Menu field restructuring:
  - Renamed "Menu (Address)" to "Menu (URL)" for clarity
  - Enhanced bilingual descriptions for menu purpose
  - Added clearer field titles and descriptions in Swedish/English

- Organization reference fields enhancement:
  - Added concrete examples (e.g., Scandic for parent organizations)
  - Improved placeholder texts with specific examples
  - Added bilingual purpose descriptions

### Documentation
- Added bilingual purpose descriptions across fields
- Enhanced field descriptions with practical examples
- Improved placeholder texts with specific guidance
- Added non-English language labels
- Improved section titles clarity

## Version 2.0.0

### Major Changes
- Extended place relation capabilities:
  - Added Landform and Guest Harbour types
  - Updated containsPlace constraints for new types
  - Restructured place relations with improved descriptions

### Schema Updates
- Changed cardinality requirements:
  - Modified facts-oriented text from pref:1 to min:0
  - Updated schema:image cardinality to recommended
- Added pattern validation for URI fields
- Enhanced constraint structure using arrays for type definitions

### New Features
- Introduced Audience functionality:
  - Added audience type selection with predefined categories:
    - Seniors
    - Students
    - Foodies
    - Environmentalists
  - Implemented age-range support (suggestedMinAge/suggestedMaxAge)
  - Added integer input validation

### Technical Enhancements
- Improved URL validation:
  - Added strict pattern: `https?:\/\/[\w\-\.]+\.\w{2,}(\/\S*)?`
  - Implemented validation across all URL fields
- Enhanced RelationForm structure
- Updated schema.org references to array format
- Added status indicators for all components

### Internationalization
- Expanded bilingual support:
  - Added Swedish translations for field purposes
  - Enhanced description fields with dual language support
  - Implemented bilingual placeholder texts
  - Added translations for field titles and help texts

### Migration Notes
1. Applications using facts-oriented text need updates due to cardinality changes
2. Place relation implementations require updates for new types
3. URL validation patterns are now strictly enforced
4. All components require bilingual support implementation
5. Applications should implement new audience-related fields
6. RDForms metadata structure support needed
7. Image handling systems need to implement license support
8. Systems must support expanded place relation constraints
9. Review existing place hierarchies for compatibility with new relation types

### Documentation
- Added explicit purpose descriptions
- Included usage examples in descriptions
- Added format specifications in placeholders
- Enhanced field documentation with bilingual support