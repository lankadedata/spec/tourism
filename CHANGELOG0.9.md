# Changelog 

### To do / to be addressed 

Change mapping - social event to dance event 

https://schema.org/FoodService

Add schema:OnlineBusiness



### Added 

schema:containedInPlace
schema:containsPlace 
specifies a relation between an entity contained within another entity schema:Place
schema:telephone with regular expression to match phone numbers starting from 7 digits
schema:price

schema:hasCredential to define a relation to e.g White Guide for accomodation, food establishment and 

schema:eventSchedule to define a recurring event
schema:byMonthWeek
schema:byDay


schema:Schedule as profile form

schema:Trip to define a packaged trip consisting of several experiences



Point-of-interest 



http://schema.org/TouristInformationCenter
http://schema.org/RecyclingCenter
http://schema.org/AutomatedTeller
http://schema.org/Pharmacy
http://schema.org/PrimaryCare
http://schema.org/Dentist
http://schema.org/Hospital
https://schema.org/TaxiStand
http://schema.org/PoliceStation
http://schema.org/ParkingFacility
http://schema.org/EmergencyService
http://schema.org/TrainStation
http://schema.org/PublicToilet






schema:IceCreamShop 
schema:Bakery 

Offer property - schema:offer 
schema:Offer
schema:description 
schema:availabilityEnds
schema:price
schema:offeredBy (lookup for organization or place)


#### Doing 

schema:LandmarksOrHistoricalBuildings 
schema:Library
schema:AmusementPark
schema:Zoo
schema:Aquarium
schema:Park
schema:Museum
schema:TennisComplex
schema:BowlingAlley
schema:SkiResort




#### Accomodation


schema:author to define the certificate authority for a specific rating
schema:ratingValue 
schema:starRating 
schema:reviewCount
schema:aggregateRating



### Changed 


schema:CampingPitch removed 

schema:maximumAttendeecapacity cardinality 

Design and shopping changed to schema:ShoppingCenter
Spa and wellness changed to schema:HealthAndBeautyBusiness

schema:Place as constraint object for the metadataprofile doing instead of schema:TouristAttraction

schema values to URI for food types and lodging types

schema:duration to only allow hours

removed priceRange and added schema:price for event and doing

schema:Place as main object for POI extension

schema:url cardinality to allow multiple links for all profiles 

schema:doorTime cardinality to recommended

category cardinality to allow multiple categories for doing

http://data.visitsweden.com/terms/FarmShop moved from eat profile to commerce profile



### Fixed 

URL for all regions 

https://www.visitsmaland.se/en/regions/kronoberg for Kronoberg 

https > http for schema:dayOfWeek


