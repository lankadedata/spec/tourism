__entryscape_config = {
    baseUrl: 'https://editera.visitsweden.com',
    entryscape: {
      static: {
        url: 'https://static.cdn.entryscape.com/',
        app: 'suite',
        version: '3.14',
      },
    },
    catalog: {
      maxFileSizeForAPI: 25000000,
      includeStatistics: false,
      includePreparations: true,
    },
    rdf: {
      namespaces: {
        visit: 'http://data.visitsweden.com/terms/',
      },
      labelProperties: [
        'schema:name',
        'schema:legalName'
      ],
    },
    entrystore: {
      repository: 'https://editera.visitsweden.com/store/',
    },
    itemstore: {
      bundles: [
        'visit2.1',
        'trails2.0'
      ],
      geonamesStart: '2661886',
      geochooserMapURL: 'https://maps.infra.entryscape.com/maps/basic-v2/256/{z}/{x}/{y}@2x.png?key=srAw96F43apXJCHhfWnu',
      geochooserMapAttribution: '<a href="https://www.maptiler.com/license/maps/" target="_blank">© MapTiler</a> <a href="https://www.openstreetmap.org/copyright" target="_blank">© OpenStreetMap contributors</a>',
    },
    site: {
      '!moduleList': ['workbench', 'test', 'documentation', 'terms', 'admin'],
      views: [
        {
          name: 'test',
          title: { en: 'The test portal', sv: 'Testportalen' },
          route: 'https://editera.visitsweden.com/apps/portal/sv/',
        },
        {
          name: 'documentation',
          title: {
            en: 'Documentation',
            sv: 'Dokumentation',
            de: 'Dokumentation',
          },
          route: 'https://docs.visitsweden.com',
        },
      ],
      modules: [
        {
          name: 'test',
          title: { en: 'Test portal', sv: 'Testportal' },
          icon: 'power',
          link: true,
          startView: 'test',
          public: true,
        },
        {
          name: "documentation",
          title: { en: 'Documentation', sv: 'Dokumentation' },
          link: true,
          startView: 'documentation',
          public: true,
        }
      ],
      signup: false,
    },
    // Keep entitytypes and projecttypes up to date with data.visitsweden.com
    entitytypes: [
      {
        name: 'region',
        label: {
          en: 'Region',
          sv: 'Region',
        },
        rdfType: 'http://schema.org/AdministrativeArea',
        template: 'visitsweden-62', // 'schema::AdministrativeArea',
        includeFile: false,
        includeLink: true,
        includeInternal: false,
        inlineCreation: false,
        allContexts: true,
        module: 'workbench',
        templateLevel: 'recommended',
        searchProps: 'schema:name',
        uriPattern: 'http://data.visitsweden.com/region/${entryName}',
        main: true
      },
      {
        name: 'action',
        label: {
          en: 'Activity',
          sv: 'Aktivitet',
        },
        rdfType: 'http://schema.org/Action',
        template: 'visitsweden-178', 
        includeFile: false,
        includeLink: true,
        includeInternal: false,
        inlineCreation: false,
        allContexts: true,
        module: 'workbench',
        templateLevel: 'recommended',
        searchProps: 'schema:name',
        uriPattern: 'http://data.visitsweden.com/region/${entryName}',
        main: true
      },
      {
        name: 'certificate',
        label: {
          en: 'Certificate',
          sv: 'Certifiering',
          },
        rdfType: 'http://schema.org/EducationalOccupationalCredential',
        template: 'visitsweden-107', // 'schema::EducationalOccupationalCredential',
        includeFile: true,
        includeLink: true,
        includeInternal: false,
        inlineCreation: false,
        allContexts: true,
        module: 'workbench',
        templateLevel: 'recommended',
        searchProps: 'schema:name',
        uriPattern: 'http://data.visitsweden.com/certificate/${entryName}',
        main: true
      },
      {
        name: 'trail',
        label: {
          en: 'Trails',
          sv: 'Leder',
        },
        rdfType: 'https://odta.io/voc/Trail',
        template: 'trails1.2-21', // 'odta::Trail',
        includeInternal: true,
        includeFile: false,
        includeLink: false,
        allContexts: true,
        inlineCreation: true,
        module: 'workbench',
        templateLevel: 'recommended',
        searchProps: 'schema:name',
        relations: [
          {
            entityType: 'image',
            type: 'aggregation',
            property: 'schema:image',
            label: {"en": "Image", 'sv': "Bild"}
          }
        ],
        publishControl: true,
        draftsEnabled: true,
      },
      {
        name: 'org',
        label: {
          en: 'Organization',
          sv: 'Organisation',
        },
        rdfType: 'http://schema.org/Organization',
        template: 'visitsweden-75', // 'schema::Organization',
        includeInternal: true,
        inlineCreation: true,
        allContexts: true,
        module: 'workbench',
        templateLevel: 'recommended',
        searchProps: 'schema:legalName',
      },
      {
        name: 'event',
        label: {
          en: 'Event',
          sv: 'Evenemang',
        },
        rdfType: 'http://schema.org/Event',
        template: 'visitsweden-83', // 'schema::Event',
        includeInternal: true,
        allContexts: true,
        module: 'workbench',
        templateLevel: 'recommended',
        searchProps: 'schema:name',
        relations: [
          {
            entityType: 'image',
            type: 'aggregation',
            property: 'schema:image',
            label: {"en": "Main image", 'sv': "Huvudbild"},
            max: 1,
          },
          {
            entityType: 'image',
            type: 'aggregation',
            property: 'schema:photo',
            label: {"en": "Image", 'sv': "Bild"}
          }
        ],
        publishControl: true,
        draftsEnabled: true,
      },
      {
        name: 'lodging',
        label: {
          en: 'Accommodation',
          sv: 'Boende',
        },
        rdfType: 'http://schema.org/LodgingBusiness',
        template: 'visitsweden-1', // 'schema::LodgingBusiness',
        includeInternal: true,
        allContexts: true,
        module: 'workbench',
        templateLevel: 'recommended',
        searchProps: 'schema:name',
        relations: [
          {
            entityType: 'image',
            type: 'aggregation',
            property: 'schema:image',
            label: {"en": "Main image", 'sv': "Huvudbild"},
            max: 1,
          },
          {
            entityType: 'image',
            type: 'aggregation',
            property: 'schema:photo',
            label: {"en": "Image", 'sv': "Bild"}
          }
        ],
        publishControl: true,
        draftsEnabled: true
      },
      {
        name: 'food',
        label: {
          en: 'Food and drinks',
          sv: 'Mat och dryck',
        },
        rdfType: 'http://schema.org/FoodEstablishment',
        template: 'visitsweden-2', // 'schema::FoodEstablishment',
        includeInternal: true,
        allContexts: true,
        module: 'workbench',
        templateLevel: 'recommended',
        searchProps: 'schema:name',
        relations: [
          {
            entityType: 'image',
            type: 'aggregation',
            property: 'schema:image',
            label: {"en": "Main image", 'sv': "Huvudbild"},
            max: 1,
          },
          {
            entityType: 'image',
            type: 'aggregation',
            property: 'schema:photo',
            label: {"en": "Image", 'sv': "Bild"}
          }
        ],
        publishControl: true,
        draftsEnabled: true,
      },
      {
        name: 'activity',
        label: {
          en: 'Attractions and places',
          sv: 'Sevärdheter och platser',
        },
        rdfType: 'http://schema.org/Place',
        template: 'visitsweden-3', // 'schema::Place',
        includeInternal: true,
        allContexts: true,
        module: 'workbench',
        templateLevel: 'recommended',
        searchProps: 'schema:name',
        relations: [
          {
            entityType: 'image',
            type: 'aggregation',
            property: 'schema:image',
            label: {"en": "Main image", 'sv': "Huvudbild"},
            max: 1,
          },
          {
            entityType: 'image',
            type: 'aggregation',
            property: 'schema:photo',
            label: {"en": "Image", 'sv': "Bild"}
          }
        ],
        publishControl: true,
        draftsEnabled: true,
      },
      {
        name: 'commerce',
        label: {
          en: 'Commerce',
          sv: 'Handel',
        },
        rdfType: 'http://schema.org/Store',
        template: 'visitsweden-54', // 'schema::Store',
        includeInternal: true,
        allContexts: true,
        module: 'workbench',
        templateLevel: 'recommended',
        searchProps: 'schema:name',
        relations: [
          {
            entityType: 'image',
            type: 'aggregation',
            property: 'schema:image',
            label: {"en": "Main image", 'sv': "Huvudbild"},
            max: 1,
          },
          {
            entityType: 'image',
            type: 'aggregation',
            property: 'schema:photo',
            label: {"en": "Image", 'sv': "Bild"}
          }
        ],
        publishControl: true,
        draftsEnabled: true
      },
      {
        name: 'trip',
        label: {
          en: "Experience",
          sv: "Upplevelsepaket",
          },
        rdfType: 'http://schema.org/Trip',
        template: 'visitsweden-111', // 'schema::Trip',
        includeInternal: true,
        allContexts: true,
        module: 'workbench',
        templateLevel: 'recommended',
        searchProps: 'schema:name',
        publishControl: true,
        draftsEnabled: true,
      },
      {
        name: 'harbour',
        label: {
          "sv": "Gästhamn",
          "en": "Guest Harbour"
        },
        rdfType: 'http://www.wikidata.org/entity/Q283202',
        template: 'visitsweden-154',
        includeInternal: true,
        allContexts: true,
        module: 'workbench',
        templateLevel: 'recommended',
        searchProps: 'schema:name',
        publishControl: true,
        draftsEnabled: true,
      },
      {
        name: 'amenity',
        label: {
          "en": "Amenities",
          "sv": "Faciliteter"
        },
        rdfType: 'http://schema.org/LocationFeatureSpecification',
        template: 'visitsweden-149',
        includeInternal: true,
        allContexts: true,
        module: 'workbench',
        templateLevel: 'recommended',
        searchProps: 'schema:name',
        uriPattern: 'http://data.visitsweden.com/amenity/${entryName}'
      },
      {
        name: 'image',
        label: { en: 'Photo', sv: 'Foto' },
        rdfType: 'http://schema.org/ImageObject',
        template: 'visitsweden-4', // 'schema::ImageObject',
        includeFile: true,
        includeLink: true,
        inlineCreation: true,
        searchProps: 'schema:name',
        split: true,
        contentviewers: ['imageview', 'metadataview'],
        module: 'workbench',
      },
      {
          name: 'resource',
          label: { en: 'Geographic resource', sv: 'Geografisk resurs' },
          rdfType: 'http://schema.org/GeoShape',
          template: 'Trails1.0-89',
          includeFile: true,
          includeLink: true,
          inlineCreation: true,
          searchProps: 'schema:name',
          module: 'workbench',
      },
    ],
    // Keep entitytypes and projecttypes up to date with data.visitsweden.com
    projecttypes: [
      {
        name: 'tourism',
        label: { en: 'Tourism', sv: 'Turism' },
        default: ['lodging', 'food', 'activity', 'commerce', 'image', 'org', 'event', 'trail', 'trip', 'harbour', 'resource'],
        optional: [],
        module: ['workbench'],
      },
      {
        name: 'amenities',
        label: { en: 'Amenities', sv: 'Faciliteter' },
        default: ['amenity'],
        optional: [],
        module: ['workbench'],
        admin: true
      },
      {
        name: 'regions',
        label: { en: 'Regions', sv: 'Regioner' },
        default: ['region'],
        optional: [],
        module: ['workbench'],
        admin: true
      },
      {
        name: 'action',
        label: { en: 'Activity', sv: 'Aktivitet' },
        default: ['action'],
        optional: [],
        module: ['workbench'],
        admin: true
      },
      {
        name: 'certificate',
        label: { en: 'Certified', sv: 'Certifiering' },
        default: ['certificate'],
        optional: [],
        module: ['workbench'],
        admin: true
      },
    ]
  };
  