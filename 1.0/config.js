rdforms_specs.init({
  language: document.targetLanguage,
  namespaces: {
    'visit': 'http://data.visitsweden.com/terms/',
  },
  bundles: [
    ['../visit0.9.json'],
  ],
  main: [
    'visitsweden-1',
    'visitsweden-2',
    'visitsweden-3',
    'visitsweden-54',
    'visitsweden-83',
    'visitsweden-136',
    'visitsweden-111'
  ],
  supportive: [
    'visitsweden-75',
    'visitsweden-4',
    'visitsweden-62',
    'visitsweden-107',
    'visitsweden-127'
  ]
});