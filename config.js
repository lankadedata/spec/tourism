rdforms_specs.init({
  language: document.targetLanguage,
  namespaces: {
    'visit': 'http://data.visitsweden.com/terms/',
  },
  bundles: [
    ['../visit2.1.json'],
    ['../trails2.0.json'],
  ],
  main: [
    'visitsweden-1',
    'visitsweden-3',
    'visitsweden-83',
    'visitsweden-2',
    'visitsweden-54',
    'visitsweden-154',
    'trails1.2-21',
    'visitsweden-111'
  ],
  supportive: [
    'visitsweden-75',
    "visitsweden-178",
    'visitsweden-4',
    'visitsweden-62',
    'visitsweden-107',
    'visitsweden-127',
    'visitsweden-149',
    "trails1.2-16",
    "trails1.2-18"
  ]
});